package com.example.batchprocessing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author user
 */
public class JasperByCollectionBeanData {
    public static void main (String[] args) throws JRException,FileNotFoundException, SQLException, ClassNotFoundException {
      Class.forName("com.mysql.jdbc.Driver");  
//      String outputFile = "C:\\Users\\user\\Desktop\\Reported" + "JasperReportExample.pdf";
//      List <Person> listItems = new ArrayList<>();
     
//      JRDataSource dataSource = new JRResultSetDataSource(null);
//JRBeanCollectionDataSource reportData = new JRBeanCollectionDataSource(listItems);

  
//Map<String,Object> parameters = new HashMap<>();

//parameters.put("condition", "LAST_NAME =  'DOE' ORDER BY FIRST_NAME ");
//parameters.put("CollectionBeanParam",reportData);

//InputStream input = new FileInputStream(new File("C:\\Users\\user\\JaspersoftWorkspace\\MyReports\\Blank_A4_1.jrxml"));

//JasperDesign jasperDesign = JRXmlLoader.load(input);

Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/person?useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false","root","181241");
String reportPath = "C:\\Users\\user\\JaspersoftWorkspace\\MyReports\\Blank_A4_1.jrxml";

JasperReport jasperReport = JasperCompileManager.compileReport(reportPath);
//JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

//JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parameters,new JREmptyDataSource());
 JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,null,con);

// pdf
//  JasperExportManager.exportReportToPdfFile(jasperPrint, "C:/Users/user/Desktop/Reported/Jasper-report.pdf");
              
//Viewer
JasperViewer.viewReport(jasperPrint);

System.out.println("File Generated");
        
    }
    
}


 
    

